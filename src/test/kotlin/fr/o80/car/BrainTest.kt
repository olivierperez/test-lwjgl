package fr.o80.car

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test


internal val linear = { x: Double -> x }
internal val delta = 0.000001

internal class BrainTest {

    @Test
    @DisplayName("Simple linear node without bias should return its input")
    fun simple() {
        // Given
        val input = Node.random(
            previousLayer = null,
            bias = 0.0,
            f = { x -> x }
        )

        // When
        input.value = 1.0
        input.compute()

        // Then
        assertEquals(1.0, input.value)
    }

    @Test
    @DisplayName("Simple linear node with bias should return its input")
    fun simpleWithBias() {
        // Given
        val input = Node.random(
            previousLayer = null,
            bias = 0.3,
            f = { x -> x }
        )

        // When
        input.value = 1.0
        input.compute()

        // Then
        assertEquals(1.3, input.value)
    }

    @Test
    @DisplayName("Activation function should be applied")
    fun checkCallOfActivationFunction() {
        // Given
        val input = Node.random(
            previousLayer = null,
            bias = 0.0,
            f = { -1000.0 }
        )

        // When
        input.value = 1.0
        input.compute()

        // Then
        assertEquals(-1000.0, input.value)
    }

    @Test
    @DisplayName("2 layers of 1 node")
    fun twoLayersOfOneNode() {
        // Given
        val input = Node.withLinks(
            previousLayer = null,
            bias = -1.0,
            f = linear
        )
        val inputLayer = Layer.fromNodes(input)

        val output = Node.withLinks(
            previousLayer = inputLayer,
            bias = 0.3,
            f = linear,
            links = listOf(1.0)
        )
        val outputLayer = Layer.fromNodes(output)


        // When
        input.value = 0.6
        inputLayer.compute()
        outputLayer.compute()

        // Then
        // 0.6 => -0.4 => -0.1
        assertEquals(-0.4, input.value)
        assertEquals(-0.1, output.value, 0.000001)
    }

    @Test
    @DisplayName("2 layers of 2 nodes")
    fun twoLayersOfTwoNodes() {
        // Given
        val input1 = Node.withLinks(
            previousLayer = null,
            bias = -1.0,
            f = linear
        )
        val input2 = Node.withLinks(
            previousLayer = null,
            bias = 0.75,
            f = linear
        )
        val inputLayer = Layer.fromNodes(input1, input2)

        val output1 = Node.withLinks(
            previousLayer = inputLayer,
            bias = 0.3,
            f = linear,
            links = listOf(1.0, 0.5)
        )
        val output2 = Node.withLinks(
            previousLayer = inputLayer,
            bias = 0.45,
            f = linear,
            links = listOf(0.8, -0.5)
        )
        val outputLayer = Layer.fromNodes(output1, output2)


        // When
        input1.value = 0.6
        input2.value = 0.1
        inputLayer.compute()
        outputLayer.compute()

        // Then
        // Input | Input layer | Output layer
        // 0.6   | -0.4        | -0.4 * 1 + 0.85 * 0.5 + 0.3 = 0.325
        // 0.1   | 0.85        | -0.4 * 0.8 + 0.85 * -0.5 + 0.45 = -0.295
        assertEquals(-0.4, input1.value, delta)
        assertEquals(0.85, input2.value, delta)
        assertEquals(0.325, output1.value, delta)
        assertEquals(-0.295, output2.value, delta)
    }

    @Test
    @DisplayName("Activation functions are applied at .compute() call")
    fun shouldExecuteActivationFunction() {
        // Given
        val input = Node.random(
            previousLayer = null,
            bias = 0.0,
            f = { x -> -x }
        )

        // When
        input.value = 1.0
        input.compute()

        // Then
        assertEquals(-1.0, input.value, delta)
    }
}