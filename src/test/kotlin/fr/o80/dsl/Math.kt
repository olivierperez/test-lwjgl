package fr.o80.dsl

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

internal class Math {

    @ParameterizedTest(name = "Test {0}")
    @MethodSource("source")
    fun `Intersection check`(letter: String, first: Vector2f, second: Vector2f, collision: Boolean) {
        assertEquals(collision, intersection(first, second, firstIsSegment = true, secondIsSegment = true) != null)
        assertEquals(collision, intersection(second, first, firstIsSegment = true, secondIsSegment = true) != null)
    }

    companion object {
        @JvmStatic
        fun source(): Stream<Arguments> {
            return Stream.of(
                Arguments.of(
                    "A",
                    Vector2f(from = Vertex2f(0f, 0f), to = Vertex2f(2f, 1f)),
                    Vector2f(from = Vertex2f(2.5f, -3f), to = Vertex2f(2.5f, 3f)),
                    false
                ),
                Arguments.of(
                    "B",
                    Vector2f(from = Vertex2f(0f, 0f), to = Vertex2f(4f, 1f)),
                    Vector2f(from = Vertex2f(2.5f, -3f), to = Vertex2f(2.5f, 3f)),
                    true
                ),
                Arguments.of(
                    "C",
                    Vector2f(from = Vertex2f(0f, 0f), to = Vertex2f(2f, 1f)),
                    Vector2f(from = Vertex2f(8f, 1f), to = Vertex2f(9f, 7f)),
                    false
                ),
                Arguments.of(
                    "D",
                    Vector2f(from = Vertex2f(6f, 0f), to = Vertex2f(2f, 1f)),
                    Vector2f(from = Vertex2f(8f, 1f), to = Vertex2f(9f, 7f)),
                    false
                ),
                Arguments.of(
                    "E",
                    Vector2f(from = Vertex2f(8f, 8f), to = Vertex2f(10f, 7f)),
                    Vector2f(from = Vertex2f(8f, 1f), to = Vertex2f(9f, 7f)),
                    false
                ),
                Arguments.of(
                    "F",
                    Vector2f(from = Vertex2f(8.5f, 6.5f), to = Vertex2f(10f, 6f)),
                    Vector2f(from = Vertex2f(8f, 1f), to = Vertex2f(9f, 7f)),
                    true
                ),
                Arguments.of(
                    "?",
                    Vector2f(from=Vertex2f(x=100.0f, y=-50.0f), to=Vertex2f(x=300.0f, y=0.0f)),
                    Vector2f(from=Vertex2f(x=99.272224f, y=-117.87187f), to=Vertex2f(x=35.68487f, y=17.983368f)),
                    false
                )
            )
        }
    }
}
