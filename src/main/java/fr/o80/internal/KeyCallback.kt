package fr.o80.internal

import org.lwjgl.glfw.GLFWKeyCallbackI

class KeyCallback : GLFWKeyCallbackI {

    private val callbacks = mutableListOf<Triple<Int, Int, () -> Unit>>()

    override fun invoke(window: Long, key: Int, scancode: Int, action: Int, mods: Int) {
        callbacks.filter { (k, a, _) -> k == key && a == action }
            .forEach { it.third() }
    }

    fun add(key: Int, action: Int, block: () -> Unit) {
        callbacks.add(Triple(key, action, block))
    }

    fun reset() {
        callbacks.clear()
    }

}