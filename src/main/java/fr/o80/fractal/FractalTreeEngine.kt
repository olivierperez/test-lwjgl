package fr.o80.fractal

import fr.o80.Engine
import fr.o80.GG
import fr.o80.Ortho
import fr.o80.dsl.*

const val width = 800
const val height = 800

class FractalTreeEngine : Engine(width, height) {

    override fun setup() {
        ortho(Ortho.BOTTOM_CENTER, GG.GL_MODELVIEW)
        draw {
            lineWidth(1f)
            color(0f, 0.5f, 0.0f)
        }
    }

    override fun update() {

    }

    override fun render() {
        draw {
            clear(0.8f, 0.8f, 0.8f, 1f)
            drawTree(200f, 20f)
        }
    }

    private fun drawTree(size: Float, angle: Float) {
        draw {
            line(
                from = Vertex3f(0f, 0f, 0f),
                to = Vertex3f(0f, size, 0f)
            )

            pushed {
                translate(0f, size, 0f)

                if (size > 2) {
                    val nextSize = size * 0.7f

                    pushed {
                        rotate(angle, 0f, 0f, 1f)
                        drawTree(nextSize, angle)
                    }

                    pushed {
                        rotate(angle, 0f, 0f, -1f)
                        drawTree(nextSize, -angle)
                    }
                }
            }
        }
    }

}