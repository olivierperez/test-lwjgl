package fr.o80.life

import kotlin.random.Random

class GameOfLife(val width: Int, val height: Int, private val tab: BooleanArray) {

    fun next(): GameOfLife {
        val next = BooleanArray(tab.size)
        for (j in 0 until height) {
            for (i in 0 until width) {
                var c = 0
                if (get(i - 1, j - 1)) c++
                if (get(i, j - 1)) c++
                if (get(i + 1, j - 1)) c++
                if (get(i - 1, j)) c++
                if (get(i + 1, j)) c++
                if (get(i - 1, j + 1)) c++
                if (get(i, j + 1)) c++
                if (get(i + 1, j + 1)) c++

                if (get(i, j)) {
                    next[toIndex(i, j)] = c == 2 || c == 3
                } else {
                    next[toIndex(i, j)] = c == 3
                }
            }
        }
        return GameOfLife(width, height, next)
    }

    operator fun get(x: Int, y: Int): Boolean {
        val wrappedX = (x + width) % width
        val wrappedY = (y + height) % height

        return tab[toIndex(wrappedX, wrappedY)]
    }

    operator fun set(x: Int, y: Int, value: Boolean) {
        val index = toIndex(x, y)
        if (index in 0..tab.size) tab[index] = value
    }

    private fun toIndex(x: Int, y: Int) = x + y * width

    companion object {
        fun random(width: Int, height: Int): GameOfLife =
            GameOfLife(
                width,
                height,
                BooleanArray(width * height) { Random.nextBoolean() })
    }

}