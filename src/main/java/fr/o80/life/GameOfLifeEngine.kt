package fr.o80.life

import fr.o80.Engine
import fr.o80.GG
import fr.o80.Ortho
import fr.o80.dsl.Vertex2i
import fr.o80.dsl.Vertex3f
import fr.o80.dsl.draw
import org.lwjgl.glfw.GLFW

const val width = 1000
const val height = 1000

class GameOfLifeEngine : Engine(width, height, 30) {

    private var game: GameOfLife = GameOfLife.random(50, 50)

    private val heightf = height.toFloat()
    private val widthf = width.toFloat()
    private val blockWidth = widthf / game.width
    private val blockHeight = heightf / game.height

    private var paused = false
    private var editing = false
    private var newValue: Boolean = false
    private var cursorPos = Vertex2i(-1, -1)

    private fun Double.toXBlock(): Int = (this / blockWidth).toInt()
    private fun Double.toYBlock(): Int = (this / blockHeight).toInt()


    override fun setup() {
        ortho(Ortho.TOP_LEFT, GG.GL_PROJECTION)
        draw {
            lineWidth(1f)
            color(0f, 0.5f, 0f)
        }

        onKey(GLFW.GLFW_KEY_SPACE, GLFW.GLFW_RELEASE) {
            paused = !paused
        }

        onMouseButton(GLFW.GLFW_MOUSE_BUTTON_1, GLFW.GLFW_PRESS) {
            editing = true
            newValue = !game[cursorPos.x, cursorPos.y]
        }

        onMouseButton(GLFW.GLFW_MOUSE_BUTTON_1, GLFW.GLFW_RELEASE) {
            editing = false
        }

        onMouseMove { x, y ->
            val xBlock = x.toXBlock()
            val yBlock = y.toYBlock()
            cursorPos = Vertex2i(xBlock, yBlock)

            if (editing) {
                game[xBlock, yBlock] = newValue
            }
        }
    }

    override fun update() {
        if (!paused)
            game = game.next()
    }

    override fun render() {
        draw {
            clear(0.8f, 0.8f, 0.8f, 1f)
            drawGrid()
            drawLife()
        }
    }

    private fun drawLife() {
        draw {
            for (j in 0 until game.height) {
                for (i in 0 until game.width) {
                    if (game[i, j]) {
                        quad(
                            a = Vertex3f(blockWidth * i, blockHeight * j, 0f),
                            b = Vertex3f(blockWidth * (i + 1), blockHeight * j, 0f),
                            c = Vertex3f(blockWidth * (i + 1), blockHeight * (j + 1), 0f),
                            d = Vertex3f(blockWidth * i, blockHeight * (j + 1), 0f)
                        )
                    }
                }
            }
        }
    }

    private fun drawGrid() {
        draw {
            (0 until game.width).forEach {
                line(
                    from = Vertex3f(it * blockWidth, 0f, 0f),
                    to = Vertex3f(it * blockWidth, heightf, 0f)
                )
            }
            (0 until game.height).forEach {
                line(
                    from = Vertex3f(0f, it * blockHeight, 0f),
                    to = Vertex3f(widthf, it * blockHeight, 0f)
                )
            }
        }
    }

}
