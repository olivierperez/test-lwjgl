package fr.o80.rain

import fr.o80.GG
import fr.o80.dsl.draw
import kotlin.random.Random

data class Drop(
    var x: Double = originX(),
    var y: Double = originY(),
    var speed: Double = originSpeed()
) {
    fun update() {
        if (y > 0)
            speed += gravity

        if (y > height) {
            y = height.toDouble()

            if (speed < 2) {
                y = originY()
                x = originX()
                speed = originSpeed()
            }

            if (y > 0)
                speed = -speed * 0.1

        }

        y += speed
    }

    fun render() {
        draw {
            GG.glPointSize(2.5f)
            color(0f, 0f, 0.6f)
            point(x, y, 0.0)
        }
    }

    companion object {
        private val random = Random(System.currentTimeMillis())
        private val originX = { random.nextDouble(width.toDouble()) }
        private val originY = { random.nextDouble(-1000.0, 0.0) }
        private val originSpeed = { random.nextDouble(5.0, 15.0) }
    }
}