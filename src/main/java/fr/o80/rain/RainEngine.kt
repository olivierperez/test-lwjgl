package fr.o80.rain

import fr.o80.Engine
import fr.o80.Ortho
import fr.o80.dsl.draw

const val gravity = 0.1
const val width = 800
const val height = 800

class RainEngine : Engine(width, height) {

    private val rain = Array(1_000) { Drop() }

    override fun setup() {
        ortho(Ortho.TOP_LEFT)
    }

    override fun update() {
        rain.forEach(Drop::update)
    }

    override fun render() {
        draw {
            clear(0.5f, 0.5f, 0.5f, 1f)
            rain.forEach(Drop::render)
        }
    }
}