package fr.o80

import fr.o80.internal.KeyCallback
import fr.o80.internal.MouseButtonCallback
import fr.o80.internal.MouseMoveCallback
import org.lwjgl.Version
import org.lwjgl.glfw.*
import org.lwjgl.opengl.GL
import org.lwjgl.system.MemoryStack
import org.lwjgl.system.MemoryUtil
import org.lwjgl.system.NativeType

enum class Ortho {
    TOP_LEFT,
    BOTTOM_CENTER,
    CENTER
}

abstract class Engine(
    private val width: Int,
    private val height: Int,
    private val updatesPerSecond: Int = 60,
    private val title: String = "Unnamed Engine"
) {

    protected var window: Long = 0
        private set

    private val keyCallback = KeyCallback()
    private val mouseButtonCallback = MouseButtonCallback()
    private val mouseMoveCallback = MouseMoveCallback()

    fun run() {
        println(Version.getVersion())

        init()
        loop(::update, ::render)

        Callbacks.glfwFreeCallbacks(window)
        GLFW.glfwDestroyWindow(window)

        GLFW.glfwTerminate()
        GLFW.glfwSetErrorCallback(null)!!.free()
    }

    private fun init() {
        GLFWErrorCallback.createPrint(System.err).set()

        if (!GLFW.glfwInit()) {
            throw IllegalStateException("Unable to initialize GLFW")
        }

        GLFW.glfwDefaultWindowHints()
        GLFW.glfwWindowHint(GLFW.GLFW_VISIBLE, GLFW.GLFW_FALSE)
        GLFW.glfwWindowHint(GLFW.GLFW_RESIZABLE, GLFW.GLFW_TRUE)

        window = GLFW.glfwCreateWindow(width, height, title, MemoryUtil.NULL, MemoryUtil.NULL)
        if (window == MemoryUtil.NULL) {
            throw IllegalStateException("Failed to create window")
        }

        onKey(GLFW.GLFW_KEY_ESCAPE, GLFW.GLFW_RELEASE) {
            GLFW.glfwSetWindowShouldClose(window, true)
        }

        GLFW.glfwSetKeyCallback(window, keyCallback)
        GLFW.glfwSetMouseButtonCallback(window, mouseButtonCallback)
        GLFW.glfwSetCursorPosCallback(window, mouseMoveCallback)

        MemoryStack.stackPush().use { stack ->
            val widthBuffer = stack.mallocInt(1)
            val heightBuffer = stack.mallocInt(1)

            GLFW.glfwGetWindowSize(window, widthBuffer, heightBuffer)

            val videoMode = GLFW.glfwGetVideoMode(GLFW.glfwGetPrimaryMonitor())!!

            GLFW.glfwSetWindowPos(
                window,
                (videoMode.width() - widthBuffer.get(0)) / 2,
                (videoMode.height() - heightBuffer.get(0)) / 2
            )
        }

        GLFW.glfwMakeContextCurrent(window)
        GLFW.glfwSwapInterval(1)

        GLFW.glfwShowWindow(window)

        GL.createCapabilities()

        GG.glMatrixMode(GG.GL_PROJECTION)

        setup()
    }

    fun ortho(ortho: Ortho, @NativeType("GLenum") mode: Int = GG.GL_PROJECTION) {
        val width = width.toDouble()
        val height = height.toDouble()

        when (ortho) {
            Ortho.TOP_LEFT -> {
                GG.glOrtho(0.0, width, height, 0.0, 0.0, 1.0)
                GG.glMatrixMode(mode)
            }
            Ortho.BOTTOM_CENTER -> {
                GG.glOrtho(-width / 2, width / 2, 0.0, height, 0.0, 1.0)
                GG.glMatrixMode(mode)
            }
            Ortho.CENTER -> {
                GG.glOrtho(-width / 2, width / 2, -height / 2, height / 2, 0.0, 1.0)
                GG.glMatrixMode(mode)
            }
        }
    }

    private fun loop(updating: () -> Unit, rendering: () -> Unit) {
        var lastTime = GLFW.glfwGetTime()
        var timer = lastTime
        var delta = 0.0
        var now: Double
        var frames = 0
        var updates = 0
        val limitFPS = 1f / updatesPerSecond

        while (!GLFW.glfwWindowShouldClose(window)) {

            now = GLFW.glfwGetTime()
            delta += (now - lastTime) / limitFPS

            lastTime = now

            while (delta > 1.0) {
                updating()
                updates++
                delta--
            }

            GG.glClear(GG.GL_COLOR_BUFFER_BIT or GG.GL_DEPTH_BUFFER_BIT)
            rendering()
            GLFW.glfwPollEvents()
            GLFW.glfwSwapBuffers(window)
            frames++

            if (GLFW.glfwGetTime() - timer > 1) {
                timer++
                println("FPS: $frames Updates: $updates")
                frames = 0
                updates = 0
            }
        }
    }

    protected fun onKey(key: Int, action: Int, block: () -> Unit) {
        keyCallback.add(key, action, block)
    }

    protected fun resetKeys() {
        keyCallback.reset()
    }

    protected fun onMouseButton(button: Int, action: Int, block: () -> Unit) {
        mouseButtonCallback.add(button, action, block)
    }

    protected fun onMouseMove(block: (Double, Double) -> Unit) {
        mouseMoveCallback.add(block)
    }

    abstract fun setup()

    abstract fun update()

    abstract fun render()
}