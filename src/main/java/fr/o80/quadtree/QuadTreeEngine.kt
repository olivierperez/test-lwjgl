package fr.o80.quadtree

import fr.o80.Engine
import fr.o80.Ortho
import fr.o80.dsl.*
import fr.o80.quadtree.lib.Point
import fr.o80.quadtree.lib.QuadTree
import fr.o80.quadtree.lib.QuadTreeImpl
import fr.o80.quadtree.lib.Rectangle
import org.lwjgl.glfw.GLFW
import kotlin.random.Random


const val width = 800
const val height = 800
const val updatesPerSecond = 30

class QuadTreeEngine : Engine(width, height, updatesPerSecond, "QuadTree") {

    private val quadTree: QuadTree = QuadTreeImpl(20, Rectangle(1f, 1f, 800f, 800f))

    private var searchingZone = Rectangle(276f, 229f, 650f, 490f)
    private var matchingPoints = emptyList<Point>()
    private var cursorPos = Vertex2f(width / 2f, height / 2f)

    private var useFakeRandom = false

    override fun setup() {
        ortho(Ortho.TOP_LEFT)

        onKey(GLFW.GLFW_KEY_SPACE, GLFW.GLFW_RELEASE) {
            useFakeRandom = !useFakeRandom
        }

        onMouseMove { x, y ->
            cursorPos = Vertex2f(x.toFloat(), y.toFloat())
        }
    }

    private fun randomPoint() {
        if (useFakeRandom) {
            val x = FakeRandom.nextFloat() * (width - 1) + 1
            val y = FakeRandom.nextFloat() * (height - 1) + 1
            quadTree.add(Point(x, y))
        } else {
            val x = Random.nextFloat() * (width - 1) + 1
            val y = Random.nextFloat() * (height - 1) + 1
            quadTree.add(Point(x, y))
        }
    }

    override fun update() {
        for (i in 1..5)
            randomPoint()

        searchingZone = Rectangle(cursorPos.x - 100, cursorPos.y - 100, cursorPos.x + 100, cursorPos.y + 100)
        matchingPoints = quadTree.intersectionWith(searchingZone)
    }

    override fun render() {
        draw {
            clear(0f, 0f, 0f)
            lineWidth(0.5f)
            pointSize(3f)

            drawQuadTree()
            drawPoints()
            drawSearchingZone()
        }
    }

    private fun Draw.drawQuadTree() {
        color(0.5f, 0.5f, 0.5f)
        quadTree.forEachTree {
            val a = Vertex3f(this.zone.min.x, this.zone.min.y, 0f)
            val b = Vertex3f(this.zone.max.x, this.zone.min.y, 0f)
            val c = Vertex3f(this.zone.max.x, this.zone.max.y, 0f)
            val d = Vertex3f(this.zone.min.x, this.zone.max.y, 0f)
            square(a, b, c, d)
        }
    }

    private fun Draw.drawPoints() {
        color(0.6f, 0.2f, 0.2f)
        quadTree.forEachPoint {
            point(x.toDouble(), y.toDouble(), 0.0)
        }
    }

    private fun Draw.drawSearchingZone() {
        val a = Vertex3f(searchingZone.min.x, searchingZone.min.y, 0f)
        val b = Vertex3f(searchingZone.max.x, searchingZone.min.y, 0f)
        val c = Vertex3f(searchingZone.max.x, searchingZone.max.y, 0f)
        val d = Vertex3f(searchingZone.min.x, searchingZone.max.y, 0f)
        color(0.2f, 1f, 0.2f)
        square(a, b, c, d)
        matchingPoints.forEach { point ->
            point(point.x.toDouble(), point.y.toDouble(), 0.0)
        }
    }
}