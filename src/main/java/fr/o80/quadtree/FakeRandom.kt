package fr.o80.quadtree

import kotlin.random.Random

object FakeRandom {

    fun nextFloat(clusters: Int = 1, force: Int = 4): Float {
        val size = clusters * 2 + 1
        var x: Float
        var remainingForce = force
        do {
            x = Random.nextFloat() * size
            remainingForce--
        } while (remainingForce > 0 && x.toInt() % 2 == 0)

        return x / size
    }
}