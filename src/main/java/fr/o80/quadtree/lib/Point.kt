package fr.o80.quadtree.lib

data class Point(val x: Float, val y: Float)
