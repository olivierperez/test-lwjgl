package fr.o80.car

import fr.o80.Engine
import fr.o80.Ortho
import fr.o80.dsl.Vertex2f
import fr.o80.dsl.draw
import java.util.*

const val width = 800
const val height = 800
const val updatesPerSecond = 30

class CarEngine : Engine(width, height, updatesPerSecond) {

    private val cars = List(5000) { createCar() }

    private var followedCar: Car? = null

    private var course: Course = CourseGenerator.generate(50)

    private val resetTask = object : TimerTask() {
        override fun run() {
            cars.forEach(Car::reset)
            course = CourseGenerator.generate(200)
        }
    }

    override fun setup() {
        ortho(Ortho.CENTER)
    }

    override fun update() {
        cars.forEach { car ->
            car.update(course)
            car.unfollow()
        }

        followedCar = cars
            .filter { !it.crashed && !it.won }
            .maxBy { it.position.x }
            ?.also(Car::follow)

        if (cars.all { it.crashed || it.won } && resetTask.scheduledExecutionTime() < System.currentTimeMillis()) {
            Timer().schedule(resetTask, 1000)
        }
    }

    override fun render() {
        draw {
            pushed {
                followedCar?.let { translate(-it.position.x, -it.position.y, 0f) }

                clear(0.5f, 0.5f, 0.5f, 1f)

                cars.forEach(Car::render)

                course.render()
            }
        }
    }

    private fun createCar(x: Float = 0f, y: Float = 0f, r: Float = 0f): Car = Car(
        position = Vertex2f(x, y),
        speed = 5f,
        rotation = r,
        size = 30f
    )
}