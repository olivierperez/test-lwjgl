package fr.o80.car

import fr.o80.dsl.Vector2f
import fr.o80.dsl.Vertex2f
import fr.o80.dsl.intersection

object VisionCalculator {
    fun compute(
        position: Vertex2f,
        rotation: Float,
        walls: List<Vector2f>,
        maxVisionDistance: Float
    ): Vision {
        val longLeftVision = Vector2f(position, position.addAngle(rotation + 0.4f, maxVisionDistance))
        val longCenterVision = Vector2f(position, position.addAngle(rotation, maxVisionDistance))
        val longRightVision = Vector2f(position, position.addAngle(rotation - 0.4f, maxVisionDistance))

        val lVision = shortVision(walls, longLeftVision, position)
        val rVision = shortVision(walls, longRightVision, position)
        val cVision = shortVision(walls, longCenterVision, position)

        return Vision(
            lVision ?: longLeftVision.to,
            cVision ?: longCenterVision.to,
            rVision ?: longRightVision.to
        )
    }

    private fun shortVision(
        walls: List<Vector2f>,
        longVision: Vector2f,
        position: Vertex2f
    ): Vertex2f? {
        return walls
            .mapNotNull { intersection(it, longVision, firstIsSegment = true, secondIsSegment = true) }
            .minBy { vision -> Vector2f(position, vision).size }
    }
}

data class Vision(val left: Vertex2f, val center: Vertex2f, val right: Vertex2f)