package fr.o80.car

import fr.o80.dsl.Vector2f
import fr.o80.dsl.Vertex3f
import fr.o80.dsl.draw

data class Course(val walls: List<Vector2f>, val end: Vector2f) {
    fun render() {
        draw {
            color(0f, 0f, 0f)
            lineWidth(2f)

            walls.forEach {
                line(
                    from = Vertex3f(it.from),
                    to = Vertex3f(it.to)
                )
            }

            color(0f, 0f, 1f)

            line(
                from = Vertex3f(end.from),
                to = Vertex3f(end.to)
            )
        }
    }
}
