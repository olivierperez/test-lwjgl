package fr.o80.car

import fr.o80.dsl.*
import kotlin.random.Random

object CourseGenerator {
    fun generate(length: Int): Course {
        val walls = mutableListOf<Vector2f>()
        var y = 0f
        var x = 0f
        var previousSize = 100f

        for (i in 0..length) {
            val previousX = x
            val previousY = y
            x += map(Random.nextFloat(), 0f, 1f, 50f, 100f)
            y += map(Random.nextFloat(), 0f, 1f, -20f, 20f)
            val newSize = constrain(
                value = previousSize + map(Random.nextFloat(), 0f, 1f, -5f, 5f),
                min = 20f,
                max = 50f)

            walls.add(
                Vector2f(
                    from = Vertex2f(previousX, previousY + previousSize),
                    to = Vertex2f(x, y + newSize)
                )
            )
            walls.add(
                Vector2f(
                    from = Vertex2f(previousX, previousY - previousSize),
                    to = Vertex2f(x, y - newSize)
                )
            )
            previousSize = newSize
        }

        val end = Vector2f(x, y - previousSize, x, y + previousSize)

        return Course(walls, end)
    }

}
