package fr.o80.car

import kotlin.math.exp
import kotlin.random.Random

private val sigmoid = { x: Double -> 2 * (1 / (1 + exp(-x)) - 0.5) }
private val softmax = { x: Double -> exp(x) / (1 + exp(x)) }
private val linear = { x: Double -> x }

class Brain
private constructor(
    private val input: Layer,
    private val layers: List<Layer>,
    private val output: Layer
) {

    fun input(
        speed: Double,
        visionLeft: Double,
        visionRight: Double,
        visionCenter: Double
    ): Decision {
        // Set input into first layer
        input.nodes[0].value = speed
        input.nodes[1].value = visionLeft
        input.nodes[2].value = visionCenter
        input.nodes[3].value = visionRight

        // Compute all layers
        layers
            .subList(1, layers.lastIndex + 1)
            .forEach { layer ->
                layer.compute()
            }

        // Return an object with output values
        return Decision(
            output.nodes[0].value.toFloat(),
            output.nodes[1].value.toFloat()
        )
    }

    companion object {
        /**
         * Create a random [Brain] with [innerLayer] inner layers of size [innerLayersSize].
         */
        fun random(innerLayer: Int, innerLayersSize: Int): Brain {
            val layers = mutableListOf<Layer>()

            // Input
            val input = Layer.random(4, null)
            layers.add(input)

            // Inner
            for (i in 0 until innerLayer) {
                layers.add(Layer.random(innerLayersSize, layers[i]))
            }

            // Output: Acceleration, Rotation
            val output = Layer.random(2, layers.last())
            layers.add(output)

            return Brain(input, layers, output)
        }

        internal fun withLayers(input: Layer, inner: List<Layer>?, output: Layer): Brain {
            val layers = mutableListOf<Layer>()
            layers.add(input)
            inner?.let(layers::addAll)
            layers.add(output)

            return Brain(input, layers, output)
        }
    }
}

data class Decision(val rotation: Float, val acceleration: Float)

class Layer private constructor(
    val nodes: List<Node>
) {

    fun compute() {
        nodes.forEach { node ->
            node.compute()
        }
    }

    companion object {
        fun random(size: Int, previousLayer: Layer?): Layer {
            return Layer(MutableList(size) { Node.random(previousLayer) })
        }

        fun fromNodes(vararg nodes: Node): Layer {
            return Layer(listOf(*nodes))
        }
    }
}

class Node
private constructor(
    previousLayer: Layer?,
    private val bias: Double,
    private val f: (Double) -> Double,
    linkValues: List<Double>
) {

    var value: Double = 0.0

    val links: List<Link>? = previousLayer?.nodes?.mapIndexed { index, node ->
        Link(linkValues[index], node)
    }

    fun compute() {
        val linksSum = links?.sumByDouble { link -> link.value() } ?: value
        value = f(bias + linksSum)
    }

    companion object {
        fun random(
            previousLayer: Layer?,
            bias: Double = 0.0,
            f: (Double) -> Double = sigmoid
        ): Node {
            val previousLayerSize = previousLayer?.nodes?.size ?: 0
            val links = (1..previousLayerSize).map { Random.nextDouble(-1.0, 1.0) }
            return Node(
                previousLayer,
                bias,
                f,
                links
            )
        }

        fun withLinks(
            previousLayer: Layer?,
            bias: Double,
            f: (Double) -> Double,
            links: List<Double> = listOf(Double.MIN_VALUE)
        ): Node {
            return Node(
                previousLayer,
                bias,
                f,
                links
            )
        }
    }
}

class Link(val bias: Double, private val node: Node) {
    fun value(): Double {
        return node.value * bias
    }

}
