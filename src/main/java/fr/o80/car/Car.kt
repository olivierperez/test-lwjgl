package fr.o80.car

import fr.o80.dsl.*
import kotlin.math.cos
import kotlin.math.sin

const val MIN_SPEED = 5f
const val MAX_SPEED = 15f
const val MAX_VISION_DISTANCE = 200f

class Car(
    var brain: Brain = newBrain(),
    val size: Float,
    var position: Vertex2f = Vertex2f(0f, 0f),
    var speed: Float = 0f,
    var rotation: Float = 0f
) {

    private lateinit var frontLeft: Vertex2f
    private lateinit var frontRight: Vertex2f
    private lateinit var backLeft: Vertex2f
    private lateinit var backRight: Vertex2f

    private lateinit var left: Vector2f
    private lateinit var right: Vector2f
    private lateinit var front: Vector2f
    private lateinit var back: Vector2f

    private lateinit var visionLeft: Vertex2f
    private lateinit var visionCenter: Vertex2f
    private lateinit var visionRight: Vertex2f

    var crashed = false
    var won = false

    private var followed = false

    init {
        computeBounds()
        computeVision(null)
    }

    fun update(course: Course) {
        if (crashed || won) return

        val decision = brain.input(
            map(speed, 0f, MAX_SPEED, -1f, 1f).toDouble(),
            map((position distanceWith visionLeft), 0f, MAX_VISION_DISTANCE, -1f, 1f).toDouble(),
            map((position distanceWith visionRight), 0f, MAX_VISION_DISTANCE, -1f, 1f).toDouble(),
            map((position distanceWith visionCenter), 0f, MAX_VISION_DISTANCE, -1f, 1f).toDouble()
        )

        // New speed
        val accelerate = map(decision.acceleration, -1f, 1f, -0.5f, 0.5f)
        speed = constrain(speed + accelerate, MIN_SPEED, MAX_SPEED)

        // New position
        val turn = map(decision.rotation, -1f, 1f, -1f, 1f)
        rotation = (rotation + turn) % (2 * PIF)
        val newX = position.x + cos(rotation) * speed
        val newY = position.y + sin(rotation) * speed
        position = Vertex2f(newX, newY)

        computeBounds()
        computeVision(course)
        computeCollisions(course)
        computeWin(course)
    }

    private fun computeWin(course: Course) {
        if (collideWith(course.end)) {
            won = true
        }
    }

    private fun computeCollisions(course: Course) {
        if (course.walls.any(::collideWith)) {
            crashed = true
        }
    }

    private fun computeBounds() {
        frontLeft = position.addAngle(rotation + 0.44f, size)
        frontRight = position.addAngle(rotation - 0.44f, size)
        backLeft = position.addAngle(rotation + PIF - 0.44f, size)
        backRight = position.addAngle(rotation + PIF + 0.44f, size)

        left = frontLeft vectorTo backLeft
        right = frontRight vectorTo backRight
        front = frontLeft vectorTo frontRight
        back = backLeft vectorTo backRight
    }

    private fun computeVision(course: Course?) {
        val (left, center, right) = VisionCalculator.compute(position, rotation, course?.walls ?: listOf(), MAX_VISION_DISTANCE)
        visionRight = right
        visionCenter = center
        visionLeft = left
    }

    private fun collideWith(wall: Vector2f): Boolean {
        return wall.collideWith(left) ||
                wall.collideWith(right) ||
                wall.collideWith(front) ||
                wall.collideWith(back)
    }

    fun render() = draw {
        if (!crashed) {
            lineWidth(2f)

            when {
                followed -> color(0.7f, 0.3f, 0.3f)
                else -> color(0.3f, 0.3f, 0.7f)
            }


            drawVision(visionLeft)
            drawVision(visionCenter)
            drawVision(visionRight)
        }

        when {
            won -> color(0.9f, 0.6f, 0.6f)
            crashed -> color(0.6f, 0.6f, 0.9f)
            followed -> color(0.7f, 0.2f, 0.2f)
            else -> color(0.2f, 0.2f, 0.7f)
        }

        quad(
            Vertex3f(frontLeft),
            Vertex3f(frontRight),
            Vertex3f(backRight),
            Vertex3f(backLeft)
        )
    }

    private fun Draw.drawVision(vision: Vertex2f) {
        line(
            from = Vertex3f(vision.x - 5, vision.y - 5, 0f),
            to = Vertex3f(vision.x + 5, vision.y + 5, 0f)
        )
        line(
            from = Vertex3f(vision.x - 5, vision.y + 5, 0f),
            to = Vertex3f(vision.x + 5, vision.y - 5, 0f)
        )
    }

    fun unfollow() {
        followed = false
    }

    fun follow() {
        followed = true
    }

    fun reset() {
        position = Vertex2f(0f, 0f)
        rotation = 0f
        speed = 5f
        crashed = false
        won = false
        brain = newBrain()
    }

    companion object {
        fun newBrain() = Brain.random(10, 10)
    }
}

